export const data = [
  {
    id: 1,
    title: 'Ipsum Dolor Sit Amet',
    img: 'https://images.unsplash.com/photo-1485827404703-89b55fcc595e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80',
    date: '09/11/2021',
    views: 120,
  },
  {
    id: 2,
    title: 'Ipsum Dolor Sit Amet',
    img: 'https://images.unsplash.com/photo-1485827404703-89b55fcc595e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80',
    date: '09/12/2022',
    views: 120,
  },
  {
    id: 3,
    title: 'Ipsum Dolor Sit Amet',
    img: 'https://images.unsplash.com/photo-1485827404703-89b55fcc595e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80',
    date: '09/12/2022',
    views: 120,
  },
  {
    id: 4,
    title: 'Ipsum Dolor Sit Amet',
    img: 'https://images.unsplash.com/photo-1485827404703-89b55fcc595e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80',
    date: '09/12/2022',
    views: 120,
  },
];
