import React from 'react';
import { Col, Row } from 'antd';
import image from '../../images/nan.jpg';
import './card.scss';
import { EyeOutlined, FieldTimeOutlined } from '@ant-design/icons';

const Card = ({ data, img = false, wrap = false }) => {
  return (
    <Col className='gutter-row' sm={24} md={12} lg={8}>
      <div className={`card__col ${wrap ? 'wrap' : ''}`}>
        {img && (
          <div className='img'>
            <img src={data.img || image} alt='' />
          </div>
        )}

        <div>
          <h4>{data.title || 'Lorem ipsum dolor sit.'}</h4>
          {wrap && (
            <>
              <span className='views'>
                <EyeOutlined className='icon' />
                {data.views}
              </span>
              <span className='line'>|</span>
            </>
          )}
          <span className='time'>
            <FieldTimeOutlined className='icon' />
            {data.date || '12/12/2022'}
          </span>
          <p>
            {data.desc ||
              'Lorem ipsum dolor sit amet consectetur adipisicing elit. Labore enim reprehenderit natus accusantium rem assumenda sint neque'}
          </p>
        </div>
      </div>
    </Col>
  );
};

export default Card;
