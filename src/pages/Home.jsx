import { Col, Row } from 'antd';
import React from 'react';
import './home.scss';
import img from '../images/nan.jpg';
import { data } from '../test';
import Card from '../components/Card/Card';

const Home = () => {
  return (
    <div className='home'>
      <Row gutter={[12, 12]}>
        {data.map((item) => (
          <Card data={item} key={item.id} img />
        ))}
      </Row>
    </div>
  );
};

export default Home;
